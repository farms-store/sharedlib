void awsLogin(String loginType,String registry){
    sh " aws ecr get-login-password --region us-east-1 | ${loginType} login --username AWS --password-stdin $registry"
}

void getVersion(){
    def version=env.BRANCH_NAME.replaceAll("/","-")
    return "${version}-${env.BUILD_NUMBER}"
}

void getTag(String version, String registry){
    def tag
    def repo = msName()
    return tag="${registry}/${repo}:${version}"
}

void msName(){
    return env.JOB_NAME.replace("CI/","").replace("/feature%2Freconfigure","")
}

void chechoutGlobalChart(){
    def remoteBranchExist=true
    sshagent(credentials: ['gitlab2']) {
        sh 'mkdir -p ~/.ssh'
        sh 'echo "Host *" > ~/.ssh/config' 
        sh 'echo "  StrictHostKeyChecking no" > ~/.ssh/config'
        sh "git config --global --add safe.directory ${WORKSPACE}/tempCheckout"
    try{
        remoteBranchExist = sh(returnStdout: true, script: "git ls-remote --heads git@gitlab.com:googlems/webstore.git | grep -w ${env.BRANCH_NAME}")
    }catch(err){
        remoteBranchExist=false
    }
    }
    sh  "git clone -b main git@gitlab.com:googlems/webstore.git ."
    if (remoteBranchExist){
        sh  "git checkout ${env.BRANCH_NAME}"
        sh  "git config pull.rebase false"
        sh  "git pull"
    }else{
        sh  "git checkout -b ${env.BRANCH_NAME}"
    }
}

void updateMsVersionInGlobalChart(String version, String msName){
    def chartYaml = readYaml file: "Chart.yaml"
    echo "values before:"
    sh "cat Chart.yaml"
    def tmp = chartYaml.version
    chartYaml['dependencies'].each{ chart -> 
        if (chart.name == msName ){
            chart.version = version            
        }  
    }   
        
        chartYaml.version = golablChartnewVersion(tmp)
        echo "The chart after: $chartYaml"
        sh 'mv Chart.yaml Chart.yaml.org'
        writeYaml file: 'Chart.yaml', data: chartYaml

}

void golablChartnewVersion(String tmp){
    //set branch name without / if not helm will fail because helm version cant have / within
    def branchName = env.BRANCH_NAME.replaceAll("/","-")
    if (env.BRANCH_NAME == "develop"){
        return "${tmp}-rc-${branchName}"
    } else if (env.BRANCH_NAME == "main"){
        return "${tmp}-${env.BUILD_NUMBER}"
    }else{
        return "${tmp}-${branchName}-${env.BUILD_NUMBER}"
    }
}

void pushGlobalChart(String registry){
    sh "rm -rf *tgz"
    sh " ls -l "
    sh " helm dep build ."
    sh " helm package ."
    sh"helm push *.tgz oci://${registry}"
    sh" rm -rf charts/"

}

void commitChanges(){
    sshagent(credentials: ['gitlabssh']) {
        sh "git config --global user.email 'aziztrainingacc@gmail.com'"
        sh "git config --global user.name 'azizaf'"
        sh "git add Chart.yaml values.yaml"
        sh "git commit -m 'jenkins update version'"
        sh "git push -f origin '${env.BRANCH_NAME}'"
    }
}