
def call(Map params = null, Closure body = null){

        properties([
            parameters([
            string(name: 'version', description: ' Helm chart version to deploy '),
            string(name: 'namespace', description: ' namespace to deploy in'),
            ])
    ])
    node(){
        try{
            if(!body){
                build(params)
            } else{
                body()
            }
        } catch(err){
            def errorMessage = err.getMessage()
            echo 'Error:' + errorMessage
        }finally {
            cleanWs()

        }
    }
}
void build(Map params = null){
    stage("deploy temp env"){
        withAWS(region:'us-east-1',credentials:'terraform-creds') {
            def registry= "174447486748.dkr.ecr.us-east-1.amazonaws.com"
            functions.awsLogin("helm registry",registry)
            try {
                sh " aws eks update-kubeconfig --name alpha-dev-app"
                sh 'curl -O "https://s3.us-west-2.amazonaws.com/amazon-eks/1.23.17/2023-03-17/bin/linux/amd64/kubectl"'  
                sh 'chmod u+x ./kubectl'                  
                sh " ./kubectl get ns ${env.namespace} || ./kubectl create ns ${env.namespace}"
                sh " helm upgrade --install ${env.namespace} oci://${registry}/webstore --version ${env.version} -n ${env.namespace}"
                sleep(10)
                sh "./kubectl patch svc frontend -n webstore -p '{\"spec\": {\"type\": \"LoadBalancer\"}}'"
                def alb=sh(script: "./kubectl get svc frontend -n webstore -ojson| jq .status.loadBalancer.ingress[0].hostname", returnStdout: true).trim().replace("\"","")
                sh "echo ${alb}"
                currentBuild.description = "fronend loadbalancer: http://${alb}"

            }catch(err){
                def errorMessage = err.getMessage()
                echo 'Error:' + errorMessage 
            }
        }
    }

}